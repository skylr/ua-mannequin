function Renderer(assets)
{
    var controls = new function()
    {   
        this.Mannequin = 'female';
    }

    var ui = new dat.GUI();
    ui.add(controls, 'Mannequin', ['female', 'male']);
    

    var scale = 0.01;

    var scene = new THREE.Scene();

    var camera = new THREE.PerspectiveCamera(5, window.innerWidth / window.innerHeight, 10, 100);

    var renderer = new THREE.WebGLRenderer({ antialias : true});
    renderer.setClearColor(new THREE.Color(0xeeeeee));
    renderer.setSize(window.innerWidth, window.innerHeight);

    var gl = renderer.context;

    camera.position.x = 25;
    camera.position.y = 0;
    camera.position.z = 0;
    camera.lookAt(scene.position);
    

	//LIGHTS
	
	//key
    var dirlight = new THREE.DirectionalLight(0x777777);
    dirlight.intensity = 1.0;
    dirlight.position.set(1, 1, 1);
    dirlight.target.position.set(0, 0, 0);
    scene.add(dirlight);
    
    //fill
    var dirlight2 = new THREE.DirectionalLight(0x777777);
    dirlight2.intensity = 0.5;
    dirlight2.position.set(-1, 0, -1);
    dirlight2.target.position.set(0, 0, 0);
    scene.add(dirlight2);
    
    //back
    var dirlight3 = new THREE.DirectionalLight(0x777777);
    dirlight3.intensity = 0.3;
    dirlight3.position.set(-1, 0, 0);
    dirlight3.target.position.set(0, 0, 0);
    scene.add(dirlight3);
    
    //screen left
    var dirlight4 = new THREE.DirectionalLight(0x777777);
    dirlight4.intensity = 0.3;
    dirlight4.position.set(0, 0, 1);
    dirlight4.target.position.set(0, 0, 0);
    scene.add(dirlight4);
    
    //front
    var dirlight5 = new THREE.DirectionalLight(0x777777);
    dirlight5.intensity = 0.3;
    dirlight5.position.set(1, 0, 0);
    dirlight5.target.position.set(0, 0, 0);
    scene.add(dirlight5);
    

    var orbitControls = new THREE.OrbitControls(camera, document.getElementById('WebGL-output'));
    orbitControls.minDistance = 22;
    orbitControls.maxDistance = 35;

    var clock = new THREE.Clock();

    document.getElementById("WebGL-output").appendChild(renderer.domElement);

    //Load env map
    var envTexture = new THREE.Texture();
    envTexture.mapping = THREE.SphericalReflectionMapping;
    envTexture.image = assets.textures.envMap;
	envTexture.needsUpdate = true;
	

	var sphereGeo = new THREE.SphereGeometry(1000, 32, 32);
 	var	sphereMat = new THREE.MeshBasicMaterial({
 		side: THREE.BackSide,
 		map: envTexture,
 	});
 	
 	sphereMesh = new THREE.Mesh(sphereGeo, sphereMat);
 	scene.add(sphereMesh);
  
    
    //mannequin mtls
	
    var femaleCoTexture = new THREE.Texture();
	femaleCoTexture.image = assets.textures.femaleCoMap;
	femaleCoTexture.needsUpdate = true;
	
	var femaleGlossTexture = new THREE.Texture();
	femaleGlossTexture.image = assets.textures.femaleGlossMap;
	femaleGlossTexture.needsUpdate = true;
	
	var femaleNmapTexture = new THREE.Texture();
	femaleNmapTexture.image = assets.textures.femaleNmapMap;
	femaleNmapTexture.needsUpdate = true;
    
    var femalemtl =  new THREE.MeshStandardMaterial({
    	map : femaleCoTexture,
    	//roughnessMap : femaleGlossTexture,
    	//normalMap : femaleNmapTexture,
    	envMap : envTexture,
    	envMapIntensity : 0.2,
		//refractionRatio : 0.0,
    	//side : THREE.DoubleSide,
    	roughness : 0.5,
    	//metalness : 0.0,
    	
    });
    
	var maleCoTexture = new THREE.Texture();
	maleCoTexture.image = assets.textures.maleCoMap;
	maleCoTexture.needsUpdate = true;
	
	var maleGlossTexture = new THREE.Texture();
	maleGlossTexture.image = assets.textures.maleGlossMap;
	maleGlossTexture.needsUpdate = true;
	
	var maleNmapTexture = new THREE.Texture();
	maleNmapTexture.image = assets.textures.maleNmapMap;
	maleNmapTexture.needsUpdate = true;
    
    
    var malemtl =  new THREE.MeshStandardMaterial({
    	//side : THREE.DoubleSide, 
    	//roughness : 0.5,
    	//metalness : 0.0,
    	map : maleCoTexture,
    	envMap : envTexture,
    	envMapIntensity : 0.2, 
    	//roughnessMap : maleGlossTexture,
    	//normalMap : maleNmapTexture,
    	//envMap : assets.cubemaps.cubemap,
    });


	//Load models
    assets.models.female_mannequin.scale.set(scale, scale, scale);
    assets.models.female_mannequin.children.forEach(function(child)
    {
        child.material = femalemtl;
    });
    scene.add(assets.models.female_mannequin);


    assets.models.male_mannequin.scale.set(scale, scale, scale);
    assets.models.male_mannequin.children.forEach(function(child)
    {
        child.material = malemtl;
    });
    scene.add(assets.models.male_mannequin);
    assets.models.male_mannequin.position.y = 0.1;

    render();


    function render()
    {
        var delta = clock.getDelta();
        orbitControls.update(delta);

        renderer.render(scene, camera);
        requestAnimationFrame(render);

        var viewInverse = new THREE.Matrix3();
        viewInverse.getInverse(camera.matrixWorldInverse);

        var scaleMatrix = new THREE.Matrix4();
        scaleMatrix.makeScale(scale, scale, scale);
        var mv = new THREE.Matrix4();
        var nmat = new THREE.Matrix3();
        mv.multiplyMatrices(camera.matrixWorldInverse, scaleMatrix);
        nmat.getNormalMatrix(mv);

		//mannequin control
        if (controls.Mannequin === 'female') {
        	model = null;
            assets.models.female_mannequin.visible = true;
            assets.models.male_mannequin.visible = false;

        	}
        else {
        	model = null;
            assets.models.female_mannequin.visible = false;
            assets.models.male_mannequin.visible = true;

        	}
		
    }
};
