function loadXHR(url, name)
{
    return new Promise(function(resolve, reject)
    {
        var req = new XMLHttpRequest();
        req.open('GET', url);

        req.onload = function()
        {
            if (req.status == 200)
            {
                resolve({ 'name' : name, 'value' : req.response});
            }
            else
            {
                reject(Error(req.statusText));
            }
        };

        req.onerror = function()
        {
            reject(Error("Network Error"));
        };

        req.send();
    });
}

function loadImage(url, name)
{
    return new Promise(function(resolve, reject)
    {
        var image = new Image();
        image.onload = function()
        {
            resolve({ 'name' : name, 'value' : image});
        }
        image.onerror = function()
        {
            reject();
        }
        image.src = url;
    });
}

function loadCubeMap(loc, name, ext)
{
    return new Promise(function(resolve, reject)
    {
        var items =
        [
            loadImage(loc + 'posx' + ext, 'posx'),
            loadImage(loc + 'negx' + ext, 'negx'),
            loadImage(loc + 'posy' + ext, 'posy'),
            loadImage(loc + 'negy' + ext, 'negy'),
            loadImage(loc + 'posz' + ext, 'posz'),
            loadImage(loc + 'negz' + ext, 'negz')
        ];
        Promise.all(items).then(function(values)
        {
            resolve({ 'name' : name, 'value' : values});
        });
    });
}

function loadOBJ(loc, obj, name, mtl, asset_name, type)
{
    return new Promise(function(resolve, reject)
    {
    	
        if (typeof mtl !== 'undefined' && mtl !== null)
        {
            var mtlloader = new THREE.MTLLoader();
            mtlloader.setBaseUrl('http://localhost:8000/');
            mtlloader.setPath(loc);
            mtlloader.load(mtl, function (materials)
            {
                materials.preload();

                var objloader = new THREE.OBJLoader();
                objloader.setMaterials(materials);
                objloader.setPath(loc);
                objloader.load(obj, function (mesh)
                {
                    resolve({'name' : name, 'value' : mesh, 'asset_name' : asset_name, 'type' : type});
                });
            });
        }
        else
        {
            var objloader = new THREE.OBJLoader();
            objloader.setPath(loc);
            objloader.load(obj, function (mesh)
            {
                resolve({'name' : name, 'value' : mesh, 'asset_name' : asset_name, 'type' : type});
            });
        }
    });
}

function loadModels()
{
    return new Promise(function (resolve, reject)
    {
        var loader = new THREE.OBJLoader();

        var items =
        [
            loadOBJ('/models/', 'male/male_mannequin.obj', 'male_mannequin', null, 'male_mannequin', 'mannequin'),
            loadOBJ('/models/', 'female/female_manequin.obj', 'female_mannequin', null, 'female_mannequin', 'mannequin'),

        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
                obj[elem.name].itemType = elem.type;
				obj[elem.name].asset_name = elem.asset_name;
                
            });
            resolve({ 'name' : 'models', 'value' : obj });
        });
    });
}

function loadTextures()
{
    return new Promise(function (resolve, reject)
    {
        var items =
        [
            //new THREE.TextureLoader().load( '/textures/orange_base_opt_default_color.jpg'), 'orangeBaseMap',
            loadImage('/textures/female_manequin_None_color.png', 'femaleCoMap'),
            loadImage('/textures/female_manequin_None_gloss.png', 'femaleGlossMap'),
            loadImage('/textures/female_manequin_None_nmap.png', 'femaleNmapMap'),
            loadImage('/textures/male_mannequin_None_color.png', 'maleCoMap'),
            loadImage('/textures/male_mannequin_None_gloss.png', 'maleGlossMap'),
            loadImage('/textures/male_mannequin_None_nmap.png', 'maleNmapMap'),
            //loadImage('/maps/pano/pano2.png', 'envMap'),
            loadImage('/maps/pano/pano3.jpg', 'envMap'),
        
        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
            });
            resolve({ 'name' : 'textures', 'value' : obj });
        });
    });
}

function loadShaders()
{
    return new Promise(function (resolve, reject)
    {
        var items =
        [
//             loadXHR('/shaders/straw.vert', 'straw_v'),
//             loadXHR('/shaders/straw.frag', 'straw_f'),
//             loadXHR('/shaders/bottle.vert', 'bottle_v'),
//             loadXHR('/shaders/bottle.frag', 'bottle_f')
        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
            });
            resolve({ 'name' : 'shaders', 'value' : obj });
        });
    });
}

function loadCubeMaps()
{
    return new Promise(function (resolve, reject)
    {
        var items =
        [
            loadCubeMap('/maps/lightskybox/', 'cubemap', '.png'),
            //loadCubeMap('/maps/refskybox/', 'cubemap', '.png'),
        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
            });
            resolve({ 'name' : 'cubemaps', 'value' : obj });
        });
    });
}
