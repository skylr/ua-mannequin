precision mediump float;

uniform samplerCube cubetex;

varying float ratio;

varying vec3 vsrefl_w;
varying vec3 vsrefr_w;

varying vec3 vspos_o;
varying vec3 vsnorm_e;
varying vec3 vsnorm_o;

uniform float u_side;

struct Material
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    vec3 emissive;
    float shininess;
};

uniform Material fmaterial;
uniform Material bmaterial;

void main()
{
    vec4 texval = textureCube(cubetex, vec3(vsrefl_w.xy, -vsrefl_w.z));
    float speclight = 1.0-texval.a;
    float blend = mix(ratio, 0.5, speclight);
    vec4 reflcol = vec4(texval.rgb, blend);

    Material material;

    float dist = sqrt((vspos_o.x*vspos_o.x) + (vspos_o.z*vspos_o.z));

    vec3 norm_e = normalize(vsnorm_e);

    if (u_side > 0.0)
    {
        material = fmaterial;

        float alpha = 0.3;

        float f = dot(norm_e, vec3(0.0, 0.0, 1.0));

        //gl_FragColor = vec4(reflcol.rgb*material.diffuse, alpha);
        //gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
        //gl_FragColor = vec4(0.002, 0.304, 0.468, 1.0);

        // main frontal area
        if (vspos_o.y > -188.6 && vspos_o.y < -4.8 && dist > 20.0)
        {
            gl_FragColor = vec4(f*material.diffuse, alpha);
        }

        // side edge treatment
        if (f < 0.2)
        {
            gl_FragColor.rgb = material.diffuse*0.25;
            gl_FragColor.a = 1.0;
        }
        else if (f < 0.3)
        {
            vec3 col = material.diffuse*0.40;
            gl_FragColor.rgb = col;
            gl_FragColor.a = 1.0;
        }
        else if (f < 0.35)
        {
            float ss = smoothstep(0.35, 0.3, f);
            vec3 col = material.diffuse*(mix(0.4, 0.7, 1.0-ss));
            gl_FragColor.rgb = col;
            gl_FragColor.a = 1.0;
        }
        else if (vspos_o.y < -189.0)
        {
            gl_FragColor.rgb = material.diffuse*0.60;
            gl_FragColor.a = 1.0;
        }

        // under cap shadow
        float topbegin = -4.2;
        float topend = -4.8;
        if (vspos_o.y > topbegin)
        {
            vec3 col = f*material.diffuse*0.20;
            gl_FragColor.rgb = col;
            gl_FragColor.a = 0.85;
        }
        else if (vspos_o.y > topend)
        {
            float ss = smoothstep(topend, topbegin, vspos_o.y);
            vec3 col = f*material.diffuse*(mix(1.0, 0.20, ss));
            gl_FragColor.rgb = col;
            gl_FragColor.a = mix(alpha, 0.85, ss);
        }

        // highlights
        gl_FragColor.rgb += vec3(speclight);
        gl_FragColor.a += 0.5*speclight;
    }
    else
    {
        material = bmaterial;

        float ndotl = dot(vec3(0.0, 0.0, 1.0), norm_e);
        gl_FragColor = vec4(reflcol.rgb*material.diffuse, 1.0);
        if (vspos_o.y > -189.6 && dist > 20.0)
        {
            gl_FragColor = vec4(material.diffuse, 1.0);
        }
        else
        {
            gl_FragColor = vec4(reflcol.rgb*0.96*material.diffuse, 1.0);
        }
        //gl_FragColor = vec4(texval.rgb, 1.0);
    }
}
