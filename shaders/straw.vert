varying vec3 vspos_o;
varying vec3 vsnorm_e;

void main() 
{
    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

    vspos_o = position;
    vsnorm_e = normalize(normalMatrix * normal);
}
