uniform vec3 bottleColor;

uniform vec3 strawColor;

uniform int flatStraw;

varying vec3 vspos_o;
varying vec3 vsnorm_e;

void main()
{
    float ff = dot(vsnorm_e, vec3(0.0, 0.0, 1.0));
    float f = 0.5*ff + 0.5;
    //vec3 col = vec3(0.5, 0.38, 0.52)*bottleColor * vec3(f);

    float topbegin = 20.0;
    float topend   = -60.0;
    float bottombegin = -70.0;
    float bottomend   = -120.0;

    //vec3 lightcol = 0.5*vec3(1.0, 0.9, 1.0)*f*bottleColor+vec3(0.3);
    //vec3 darkcol = f*strawColor;
    vec3 lightcol = f*strawColor+vec3(0.1);
    vec3 darkcol = 0.5*f*strawColor;

    if (flatStraw == 1)
    lightcol = 0.8*f*strawColor + vec3(0.1);

    vec3 col;
    if (vspos_o.y < topend && vspos_o.y > bottombegin)
    {
        col = lightcol;
    }
    else if (vspos_o.y <= bottombegin && vspos_o.y > bottomend)
    {
        float ss = smoothstep(bottombegin, bottomend, vspos_o.y);
        col = mix(lightcol, darkcol, ss);
    }
    else if (vspos_o.y <= topbegin && vspos_o.y > topend)
    {
        float ss = smoothstep(topend, topbegin, vspos_o.y);
        col = mix(lightcol, darkcol, ss);
    }
    else // below bottomend
    {
        col = darkcol;
    }
    gl_FragColor.rgb = col;
    gl_FragColor.a = 1.0;
}
