precision mediump float;
attribute vec3 pos_o;
attribute vec3 norm_o;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;
uniform mat3 viewInverseMatrix;

uniform float u_side;

varying float ratio;
varying vec3 vsrefl_w;
varying vec3 vsrefr_w;

varying vec3 vspos_e;
varying vec3 vsnorm_e;
varying vec3 vsnorm_o;

varying vec3 vspos_o;

void main(void)
{
    float n_air = 1.000293;
    float n_glass = 1.58;
    float eta = n_air / n_glass;
    float fresnel_power = 5.0;

    float F = ((1.0-eta) * (1.0-eta)) / ((1.0+eta) * (1.0+eta));

    vec3 norm_e = normalize(normalMatrix * u_side*norm_o);

    vec4 pos_e = modelViewMatrix * vec4(pos_o, 1.0);
    pos_e /= pos_e.w;
    vec3 i_e = normalize(pos_e.xyz);

    vsrefl_w = reflect(i_e, norm_e);
    vsrefr_w = refract(i_e, norm_e, eta);

    ratio = F + (1.0 - F) * pow((1.0 - dot(-i_e, norm_e)), fresnel_power);

    vspos_e = pos_e.xyz;
    vsnorm_e = norm_e;
    vsnorm_o = norm_o;

    vspos_o = pos_o;

    gl_Position = projectionMatrix * modelViewMatrix * vec4(pos_o, 1.0);
}
